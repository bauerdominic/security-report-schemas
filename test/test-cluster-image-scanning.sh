#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SCHEMA="$PROJECT_DIRECTORY/dist/cluster-image-scanning-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"
source "$PROJECT_DIRECTORY/test/common-tests.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_cluster_image_scanning_contains_common_definitions() {
  ensure_common_definitions "$SCHEMA" '["cluster_image_scanning"]'
}

test_cluster_image_scanning_extensions() {
  verify_schema_contains_selector "$SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.image"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.kubernetes_resource"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.kubernetes_resource.properties.namespace"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.kubernetes_resource.properties.kind"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.kubernetes_resource.properties.container_name"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.kubernetes_resource.properties.name"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency.properties.package.properties.name"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency.properties.version"
  verify_schema_contains_selector "$SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.dependency.properties.package"
}
