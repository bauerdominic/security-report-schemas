#!/usr/bin/env bash

set -e

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SRC_DIRECTORY="$PROJECT_DIRECTORY/src"
DIST_DIRECTORY="$PROJECT_DIRECTORY/dist"

# Required so that node_modules is found in the correct location
cd "$PROJECT_DIRECTORY"

for src in $(find "$SRC_DIRECTORY" -name '*.json' -maxdepth 1 | grep -v security-report-format.json | grep -v vulnerability-details-format.json); do
  dst=$(basename "$src")
  dst="$DIST_DIRECTORY/$dst"
  echo "Creating $dst"
  node -r esm bin/merge/index.js "$src" | jq '.' >"$dst"
done

echo "Done"
